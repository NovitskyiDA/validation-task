require './validation_module'

class User; end

class Animal
  include Validation

  attr_reader :name, :legs, :owner

  def initialize(name, legs, owner)
    @name = name
    @legs = legs
    @owner = owner
  end

  validate :name, presence: true
  validate :legs, format: /A-Z{0,3}/
  validate :owner, type: User
end
