module Validation
  @@errors = []

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def self.options
      @options ||= {}
    end

    def validate(field, validation)
      ClassMethods.options[field] = validation
    end
  end
  
  def validate!
    ClassMethods.options.each do |field, options|
      options.each do |key, value|
        case key
        when :presence
          @@errors.push("#{field} can't be empty") if value && public_send(field).nil?
        when :format
          @@errors.push("#{field} doesn't match the format") unless public_send(field).match?(value)
        when :type
          @@errors.push("#{field} type isn't correct") unless public_send(field) == value
        end
      end
    end

    @@errors.each { |error| puts error } if @@errors.any?
  end

  def valid?
    @@errors.empty?
  end
end
